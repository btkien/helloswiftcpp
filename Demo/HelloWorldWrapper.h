//
//  HelloWorldWrapper.h
//  SwiftCPP
//
//  Created by Anurag Ajwani on 14/09/2020.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HelloWorldWrapper : NSObject
- (NSString *) sayHello;
@end

NS_ASSUME_NONNULL_END
