//
//  ImageFilters.h
//  ImageFilters
//
//  Created by ernesto on 15/06/13.
//  Copyright (c) 2013 raywenderlich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Painter : NSObject
- (void) init;
- (void) paintRandomColor;
@end

NS_ASSUME_NONNULL_END
